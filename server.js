var server = require('http').createServer(handler);
var io = require('socket.io').listen(server);
var fs = require('fs');
var App = require('./app.js').App;
var app = new App(io.sockets);

server.listen(8000);

io.configure(function() {
    io.set('transports', ['websocket']);
    io.set('log level', 2);
});

// Handles inital brower based communication
function handler(req, res){
    fs.readFile(__dirname + '/index.html',
        function (err, data){
            if(err){
                res.writeHead(500);
                return res.end('Error loading index.html');
            }
            console.log("connection attempt");
            res.writeHead(200);
            res.end(data);
        });
}
// Reads server stdin inputs for console commands
process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', function(data){ app.processCommand(io.sockets, data)});



// Handles socket connection messages
io.sockets.on('connection', function(client) {
    client.on('login',      function(data){app.login(client, data) });
    client.on('play',       function(data){app.play(client, data)});
    client.on('message',    function(data){app.message(client, data)});
    client.on('disconnect', function(data){app.disconnect(client, data)});
    client.on('join',       function(data){app.join(client, data)});
    client.on('leave',      function(data){app.leave(client, data)});
    client.on('create',     function(data){app.create(client, data)});
    client.on('list',       function(data){app.list(client, data)});
    client.on('help',       function(data){app.help(client, data)});
    client.on('whisper',    function(data){app.whisper(client, data)});
});





