var io = require('socket.io/node_modules/socket.io-client');
var socket = io.connect('http://localhost:8000', { 'connect timeout': 5000});

socket.on('error', function(){
    console.log("connection failed");
});
socket.on('connect', function(){
    console.log("username " +process.argv[2]);
    socket.emit('login', process.argv[2]);

    socket.on('event', function(data){});
    socket.on('message', function(data){
        console.log(data);
    });
    socket.on('disconnect', function(){
        console.log("disconnected");
    });
    socket.on('list', function(data){
        for(var i = 0; i < data.length; i++){
            console.log(data[i]);
        }
    });
});



process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', function (chunk) {
    message = chunk.toString().trim();
    if(message.charAt(0) == '/'){
        message = message.substr(1);
        message = message.split(' ');
        command = message.shift();
        message = message.join(' ');
        socket.emit(command, message);
    } else {
        socket.emit('message', message);
    }
});

