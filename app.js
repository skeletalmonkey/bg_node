var User = require('./user.js').User;
var Room = require('./room.js').Room;
var settings = require('./settings.js').settings;


function App(sockets){
    this.instructions = "Type /join [roomid] to create a new room or join an existing one\n" +
        "Type /help for more help";
    this.room_key_index = 0;
    this.users = {};
    this.rooms = {};
    this.usernames = {};
    this.sockets = sockets;
}
App.prototype = {

    help : function(client, data){
        help_message = "User " + this.users[client.id].name + " is currently in ";
        if(this.users[client.id].room == null){
            help_message += "the lobby";
        } else {
            help_message += "room " + this.users[client.id].room + "\n";
        }
        help_message += "/join [room_number] - joins a room \n";
        help_message += "/leave - leaves the room you're in\n";
        help_message += "/createroom - creates a new room\n";
        help_message += "/list [users|rooms] - lists the users|rooms\n";
        help_message += "/list [users|rooms] - lists the users|rooms\n";
        client.emit('message', help_message);
    },

    whisper : function(client, data){
        name = data.substring(0, data.indexOf(' '));
        if(this.usernames[name]){
            message = data.substr(data.indexOf(' '));
            this.users[this.usernames[name]].socket.emit('message', this.users[client.id].name + ": " + message);
        } else {
            client.emit('message', "User " + name + " does not exist");
        }
    },



    // Handles all socket messages
    processCommand : function(sockets, chunk){
        data = chunk.toString().trim();
        arg = data.split(':');
        switch(arg[0]){
            case "chat":
                if(arg[2] == null){
                    sockets.emit('message', arg[1]);
                } else {
                    sockets.in(arg[2]).emit('message', arg[1]);
                }
                break;
            case "users":
                console.log(this.users);
                break;
            default:
                console.log("not a command");
        }
    },

    // Handles socket connection
    login : function(client, user){
        this.users[client.id] = new User(client, user);
        this.usernames[user] = client.id;
        console.log("User " + user + " with id:" +client.id+ " has connected");
        client.emit('message', "Connect to Skelton's Game server. Welcome " +user+"!");
        client.emit('message', this.instructions);
    },

    disconnect : function(client, data){
        console.log("User " + this.users[client.id].name + " with id:" +client.id+ " has disconnected");
        delete this.users[client.id];
    },


    play : function(client, data){
    },

    message : function (client, data) {
        console.log(this.users[client.id].name + ": " + data);
    },

    // Creates a room with an instance of a game
    create : function (client, data) {
        if(this.users[client.id].room != null){
            client.emit('message', "You are already in room " + this.users[client.id].room + ". Type /leave to exit room");
            return;
        } else if(!settings.game_list[data]){
            client.emit('message', "Game does not exist, please pick from '/list games'");
        }
        // Get free room ID
        while(this.rooms[this.room_key_index]){
            console.log(this.room_key_index);
            this.room_key_index += 1;
            this.room_key_index %= 9999;
        }
        // Create room and game
        room = new Room(this.room_key_index, this.users[client.id].name, client.id, settings.game_list[data]);
        room.game = new settings.game_list[data].game(client.id);

        this.rooms[room.key] = room;
        this.users[client.id].room = room.key;
        client.join(room.key);

        console.log(this.users[client.id].name +" has created room " + room.title());
        client.emit('message', "Entered new room " + room.title()); 
        client.emit('message', "Waiting for " + (settings.game_list[data].players - 1) + " more player(s) to join");
    },

    // Room handling
    join : function (client, data){
        key = Number(data);
        if(data == ""){
            client.emit('message', "Please enter a room number to join or use /create to create a new room");
        } else if(this.users[client.id].room != null){
            client.emit('message', "You are already in room " + this.users[client.id].room + ". Type /leave to exit room");
        } else if(isNaN(key)){
            client.emit('message', "Please enter a valid room number");
        } else if(!this.rooms[key]){
            client.emit('message', "Room number " + room_key + " does not exist. Create a room with /create");
        } else if(this.rooms[key].freeSeats() == 0){
            client.emit('message', "Sorry there are no free seats in this game");
        } else {
            console.log(this.rooms[key]);
            result = this.rooms[key].game.sit(client.id);
            client.emit('message', result.message);
            if(result.code == 0){
                this.sockets.in(key).emit('message', this.users[client.id].name + " has joined the game");
                client.join(key);
                this.users[client.id].room = key;
                this.rooms[key].users[client.id] = this.users[client.id];
                client.emit('message', "Joined room " + key);
                console.log("User " + this.users[client.id].name + " has joined room " + this.rooms[key].title());
            }
        }
    },

    leave : function (client, data){
        if(this.users[client.id].room == null){
            client.emit('message', "You are not currently in a room");
        } else {
            room_key = this.users[client.id].room;
            if(this.rooms[room_key].owner_id == client.id){
                client.emit('message', "You are the current owner of this room. Transfer power with /handover or delete room with /delete");
            } else {
                client.leave(room_key);
                delete this.rooms[room_key].users[client.id];
                this.users[client.id].room = null;
                client.emit('message', "Leaving room " + room_key);
                console.log(this.users[client.id].name + " has left room " + room_key);


            }
        }
    },






    list : function(client, data) {
        console.log("List request for " + data + " from " + this.users[client.id].name);
        temp = [];
        switch (data){
            case "users" :
                for(key in this.users){
                    console.log(key);
                    temp.push(this.users[key].name);
                }
                break;
            case "rooms" :
                for(key in this.rooms){
                    temp.push(this.rooms[key].key + " owned by " + this.rooms[key].owner);
                }
                if(temp.length == 0){
                    temp.push("No rooms");
                }
                break; 
            case "games" :
                for(key in settings.game_list){
                    temp.push(key + ": " + settings.game_list[key].name);
                }
                if(temp.length == 0){
                    temp.push("No games");
                }
                break;
            default :
                client.emit('message', "List only handles options users, rooms and games");
        }
        
        client.emit('list', temp);
    },



}

exports.App = App;


