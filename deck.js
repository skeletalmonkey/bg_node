function Stack() {
    this.stack = [];
}

// The top of the stack is at index 0
Stack.prototype = {
    init : function(cards){
        this.stack = cards;
    },

    addBottom : function(cards){
        if(cards instanceof Array){
            this.stack = this.stack.concat(cards);
        } else {
            this.stack.push(cards);
        }
    },

    addTop : function(cards){
        if(cards instanceof Array){
            this.stack = cards.concat(this.stack);
        } else {
            this.stack.unshift(cards);
        }
    },
    addSorted : function(cards){
        if(cards instanceof Array){
            this.stack = cards.concat(this.stack);
        } else {
            this.stack.push(cards);
        }
        this.stack.sort(function(a,b){ return a.order - b.order});
    },

    size : function(){
        return this.stack.length;
    },

    shuffle : function(){
        for(var i = this.stack.length-1; i > 0; i--){
            j = Math.floor(Math.random()*i);
            temp = this.stack[i];
            this.stack[i] = this.stack[j];
            this.stack[j] = temp;
        }
    },

    get : function(){
        return this.stack;
    },

    top : function(){
        if(this.stack.length == 0){
            return null;
        } else {
            return this.stack[0];
        }
    },
    bottom : function(){
        return this.stack[this.stack.length - 1];
    },
    takeTop : function(){
        return this.stack.shift();
    }
}
function Deck(){
    this.stack = [];
}
Deck.prototype = new Stack();
Deck.prototype.draw = function(n){
        // If no argument draw one card
        if(n == undefined){
            n = 1;
        }
        if(this.stack.length <= n){
            console.log("Not enough cards");
            return null;
        } else {
            var temp = [];
            for( i = 0; i < n; i++){
                temp.push(this.stack.shift());
            }
            return temp;
        }
    }
function Hand() {
    this.stack = [];
}
Hand.prototype = new Stack();
Hand.prototype.play = function(n){
        if(this.stack.length < n){
            return "Not enough cards";
        } else {
            var temp = this.stack.splice(n-1, 1);
            return temp[0];
        }
    }
exports.Stack = Stack;
exports.Deck = Deck;
exports.Hand = Hand;
