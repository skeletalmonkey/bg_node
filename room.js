function Room(key, name, id, settings) {
    this.key = key;
    this.owner = name;
    this.owner_id = id;
    this.users = {};
    this.num_users = 1;
    this.game = null;
    this.settings = settings;
}
Room.prototype = {

    title: function(){
        return "#" + this.key+": "+this.settings.name+" ("+ this.num_users+"/"+this.settings.players+")";
    },

    freeSeats: function(){
        return this.settings.players - this.num_users;
    }
}


exports.Room = Room;
