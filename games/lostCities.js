var Deck = require('../deck.js').Deck;
var Hand = require('../deck.js').Hand;

function Game(player1){
    this.deck = new Deck();
    this.discards = [];

    this.players = [];
    this.players[player1] = {};
    this.player1 = this.players[player1];
    this.player2 = null;
}

Game.prototype = {

    // returns tuple where first is status, and second is message that gets sent back to player.
    // 0 : success
    // 1 : failure
    // -1 : still waiting
    sit : function(player){
        if(this.player2 != null){
            return {code: 1, message:"Sorry this game is full"};
        } else {
            this.players[player] = {};
            this.player2 = this.players[player];
            this.init();
            return {code: 0, message:"You are player 2"};
        }


    },

    init : function(){
        this.discards['B'] = new Deck();
        this.discards['G'] = new Deck();
        this.discards['R'] = new Deck();
        this.discards['W'] = new Deck();
        this.discards['Y'] = new Deck();

        this.player1.hand = new Hand();
        this.player1.stacks = [];
        this.player1.stacks['B'] = new Deck();
        this.player1.stacks['G'] = new Deck();
        this.player1.stacks['R'] = new Deck();
        this.player1.stacks['W'] = new Deck();
        this.player1.stacks['Y'] = new Deck();

        this.player2.hand = new Hand();
        this.player2.stacks = [];
        this.player2.stacks['B'] = new Deck();
        this.player2.stacks['G'] = new Deck();
        this.player2.stacks['R'] = new Deck();
        this.player2.stacks['W'] = new Deck();
        this.player2.stacks['Y'] = new Deck();

        this.deck.init([ 
                { suit : 'B' , value : 0 , order: 0} ,{ suit : 'B' , value : 0 , order: 1} ,{ suit : 'B' , value : 0 , order: 2} ,{ suit : 'B' , value : 1 , order: 3} ,
                { suit : 'B' , value : 2 , order: 4} ,{ suit : 'B' , value : 3 , order: 5} ,{ suit : 'B' , value : 4 , order: 6} ,{ suit : 'B' , value : 5 , order: 7} ,
                { suit : 'B' , value : 6 , order: 8} ,{ suit : 'B' , value : 7 , order: 9} ,{ suit : 'B' , value : 8 , order: 10} ,{ suit : 'B' , value : 9 , order: 11} ,
                { suit : 'B' , value : 10 , order: 12},
                { suit : 'G' , value : 0 , order: 13} ,{ suit : 'G' , value : 0 , order: 14} ,{ suit : 'G' , value : 0 , order: 15} ,{ suit : 'G' , value : 1 , order: 16} ,
                { suit : 'G' , value : 2 , order: 17} ,{ suit : 'G' , value : 3 , order: 18} ,{ suit : 'G' , value : 4 , order: 19} ,{ suit : 'G' , value : 5 , order: 20} ,
                { suit : 'G' , value : 6 , order: 21} ,{ suit : 'G' , value : 7 , order: 22} ,{ suit : 'G' , value : 8 , order: 23} ,{ suit : 'G' , value : 9 , order: 24} ,
                { suit : 'G' , value : 10 , order: 25} ,
                { suit : 'R' , value : 0 , order: 26} ,{ suit : 'R' , value : 0 , order: 27} ,{ suit : 'R' , value : 0 , order: 28} ,{ suit : 'R' , value : 1 , order: 29} ,
                { suit : 'R' , value : 2 , order: 30} ,{ suit : 'R' , value : 3 , order: 31} ,{ suit : 'R' , value : 4 , order: 32} ,{ suit : 'R' , value : 5 , order: 33} ,
                { suit : 'R' , value : 6 , order: 34} ,{ suit : 'R' , value : 7 , order: 35} ,{ suit : 'R' , value : 8 , order: 36} ,{ suit : 'R' , value : 9 , order: 37} ,
                { suit : 'R' , value : 10 , order: 38} ,
                { suit : 'W' , value : 0 , order: 39} ,{ suit : 'W' , value : 0 , order: 40} ,{ suit : 'W' , value : 0 , order: 41} ,{ suit : 'W' , value : 1 , order: 42} ,
                { suit : 'W' , value : 2 , order: 43} ,{ suit : 'W' , value : 3 , order: 44} ,{ suit : 'W' , value : 4 , order: 45} ,{ suit : 'W' , value : 5 , order: 46} ,
                { suit : 'W' , value : 6 , order: 47} ,{ suit : 'W' , value : 7 , order: 48} ,{ suit : 'W' , value : 8 , order: 49} ,{ suit : 'W' , value : 9 , order: 50} ,
                { suit : 'W' , value : 10 , order: 51} ,
                { suit : 'Y' , value : 0 , order: 52} ,{ suit : 'Y' , value : 0 , order: 53} ,{ suit : 'Y' , value : 0 , order: 54} ,{ suit : 'Y' , value : 1 , order: 55} ,
                { suit : 'Y' , value : 2 , order: 56} ,{ suit : 'Y' , value : 3 , order: 57} ,{ suit : 'Y' , value : 4 , order: 58} ,{ suit : 'Y' , value : 5 , order: 59} ,
                { suit : 'Y' , value : 6 , order: 60} ,{ suit : 'Y' , value : 7 , order: 61} ,{ suit : 'Y' , value : 8 , order: 62} ,{ suit : 'Y' , value : 9 , order: 63} ,
                { suit : 'Y' , value : 10 , order: 64}
        ]);
        this.deck.shuffle();
        this.player1.hand.addSorted(this.deck.draw(8));
        this.player2.hand.addSorted(this.deck.draw(8));
    },

    play : function(player, action){
        player = this.players[player];
        playedCard = player.hand.play(action.playedCard);
        if(action.discard == true){
            if(this.discards[playedCard.suit].size == 0){
                return "Invalid Move: "+playedCard.suit+" discard pile is empty.";
            } else {
                this.discards[playedCard.suit].addTop(playedCard);
            }
        } else {
            if(player.stacks[playedCard.suit].size > 0 && player.stacks[playedCard.suit].top().value < playedCard.value){
                return "Invalid Move: Must play cards in increasing value.";
            } else {
                player.stacks[playedCard.suit].addTop(playedCard);
            }
        }
        if(action.drawFrom == 'D'){
            player.hand.addSorted(this.deck.draw());
            if(this.deck.size() == 0){
                return "Game Over";
            }
        } else {
            if(this.discards[action.drawFrom].size == 0){
                return "Invalid Move: "+action.drawFrom+" discards pile is empty";
            }
            player.hand.addSorted(this.discards[action.drawFrom].takeTop());

        }
        return "Success";

    },

    getBoard : function(){
        board = {};
        board.deck_size = this.deck.size();
        board.discards = { 'B' : this.discards['B'].top(), 
            'G' : this.discards['G'].top(),
            'R' : this.discards['R'].top(),
            'W' : this.discards['W'].top(),
            'Y' : this.discards['Y'].top()  }
        board.p1Stacks = { 'B' : this.player1.stacks['B'].get(), 
            'G' : this.player1.stacks['G'].get(),
            'R' : this.player1.stacks['R'].get(),
            'W' : this.player1.stacks['W'].get(),
            'Y' : this.player1.stacks['Y'].get()  }
        board.p2Stacks = { 'B' : this.player2.stacks['B'].get(), 
            'G' : this.player2.stacks['G'].get(),
            'R' : this.player2.stacks['R'].get(),
            'W' : this.player2.stacks['W'].get(),
            'Y' : this.player2.stacks['Y'].get()  }
        board.p1Hand = this.player1.hand.get();
        board.p2Hand = this.player2.hand.get();
        return board;
    },

    logBoard : function(){
        board = this.getBoard();
        row = [];
        for(var key in board.discards){
            row[key] = key + ":";
        }
        // Print player 1
        for(var i = 12; i >= 0; i--){
            for(var j in board.p1Stacks){
                if(board.p1Stacks[j].length >= i+1){
                    row[j] += board.p1Stacks[j][board.p1Stacks[j].length-(i+1)].value + " ";
                } else {
                    row[j] += "  ";
                }
            }
        }

        // Print discards
        for(var j in board.discards){
            row[j] += ":";
            row[j] += board.discards[j] == null ? '.' : board.discards[j].value;
            row[j] += ":";
        }
        for(var i = 0; i <= 12; i++){
            for(var j in board.p2Stacks){
                if(board.p2Stacks[j].length >= i+1){
                    row[j] += board.p2Stacks[j][i].value + " ";
                } else {
                    row[j] += "  ";
                }
            }
        }



        for(var key in board.discards){
            console.log(row[key]);
        }
    }

}           


exports.Game = Game; 
