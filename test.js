var Deck = require('./deck.js').Deck;
var Hand = require('./deck.js').Hand;
var Game = require('./lostCities.js').Game;
var deck = new Deck();
var hand = new Hand();
var game = new Game(1, 2);

console.log(game.getBoard());
game.logBoard();



// Console command code
process.stdin.resume();
process.stdin.setEncoding('utf8');
data = "";
process.stdin.on('data', function (chunk) {
    data = chunk.toString().trim();
    arg = data.split(' ');
    switch(arg[0]){
        case "shuffle":
            deck.shuffle();
            console.log(deck.print());
            break;
        case "draw":
            hand.addTop(deck.draw());
            console.log("Deck: " + deck.print());
            console.log("Hand: " + hand.print());
            break;
        case "print":
            game.logBoard();
            break;
        case "play":
            action = {};
            action.playedCard = arg[2];
            action.discard = arg[3] == 'D';
            action.drawFrom = arg[4] ? arg[4] : 'D';
            console.log(game.play(arg[1], action));

            console.log(game.getBoard());
            console.log(game.getBoard());
            game.logBoard();
            break;
            
        default:
            console.log("not a command");
    }


});


