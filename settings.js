var LostCities = require('./games/lostCities.js').Game;

var settings = {
    game_list : { 
        1 : 
            { name : "Lost Cities",
              path : './games/lostCities.js',
              players : 2,
              game : LostCities
            }
    }
}

exports.settings = settings;
