function User(client, name) {
    this.id = client.id;
    this.socket = client;
    this.name = name;
    this.room = null;
}

exports.User = User;
